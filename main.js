const getRandomItem = (xs, roll=Math.random()) =>
	xs.reduce((acc, [prompt, odds]) => odds < roll ? prompt : acc, '')

const getRandomInt = (min, max) =>
	(Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + Math.ceil(min))

const toEqualOdds = xs =>
	xs.map((str, i) => ([str, i > 0 ? i * (1 / xs.length)  : 0]))

const SCENE_COMPLICATION = toEqualOdds([
	'Hostile forces oppose you',
	'An obstacle blocks your way',
	'Wouldn\'t it suck if...',
	'An NPC acts suddenly',
	'All is not as it seems',
	'Things actually go as planned',
])

const ALTERED_SCENE = toEqualOdds([
	'RANDOM_EVENT',
	'A major detail of the scene is enhanced or somehow worse',
	'The environment is different',
	'Unexpected NPCs are present',
	'SCENE_COMPLICATION',
	'PACING_MOVE',
])

const PACING_MOVE = toEqualOdds([
	'Foreshadow trouble',
	'Reveal a new detail',
	'An NPC takes action',
	'Advance a threat',
	'Advance a plot',
	'RANDOM_EVENT',
])

const FAILURE_MOVE = toEqualOdds([
	'Cause harm',
	'Put someone in a spot',
	'Offer a choice',
	'Advance a threat',
	'Reveal an unwelcome truth',
	'RANDOM_EVENT',
])

const SUIT_DOMAIN = toEqualOdds([
	'Physical (appearance, existence)',
	'Technical (mental, operation)',
	'Mystical (meaning, capability)',
	'Social (personal, connection)',
])

const ACTION_FOCUS = toEqualOdds([
	'Seek',
	'Oppose',
	'Communicate',
	'Move',
	'Harm',
	'Create',
	'Reveal',
	'Command',
	'Take',
	'Protect',
	'Assist',
	'Transform',
	'Deceive',
])

const DETAIL_FOCUS = toEqualOdds([
	'Small',
	'Large',
	'Old',
	'New',
	'Mundane',
	'Simple',
	'Complex',
	'Unsavory',
	'Specialized',
	'Unexpected',
	'Exotic',
	'Dignified',
	'Unique',
])

const TOPIC_FOCUS = toEqualOdds([
	'Current need',
	'Allies',
	'Community',
	'History',
	'Future plans',
	'Enemies',
	'Knowledge',
	'Rumors',
	'A plot arc',
	'Recent events',
	'Equipment',
	'A faction',
	'The PCs',
])

const OBJECTIVE = toEqualOdds([
	'Eliminate a threat',
	'Learn the truth',
	'Recover something valuable',
	'Escort or deliver to safety',
	'Restore something broken',
	'Save an ally in peril',
])

const ADVERSARIES = toEqualOdds([
	'A powerful organization',
	'Outlaws',
	'Guardians',
	'Local inhabitants',
	'Enemy horde or force',
	'A new or recurring villian',
])

const REWARDS = toEqualOdds([
	'Money or valuables',
	'Knowledge and secrets',
	'Support of an ally',
	'Advance a plot arc',
	'A unique item of power',
])

const IDENTITY = toEqualOdds([
	'Outlaw',
	'Drifter',
	'Tradesman',
	'Commoner',
	'Soldier',
	'Merchant',
	'Specialist',
	'Entertainer',
	'Adherent',
	'Leader',
	'Mystic',
	'Adventurer',
	'Lord',
])

const GOAL = toEqualOdds([
	'Obtain',
	'Learn',
	'Harm',
	'Restore',
	'Find',
	'Travel',
	'Protect',
	'Enrich self',
	'Avenge',
	'Fulfill duty',
	'Escape',
	'Create',
	'Serve',
])

const NOTABLE_FEATURES = toEqualOdds([
	'Unremarkable',
	'Notable nature',
	'Obvious physical trait',
	'Quirk or mannerism',
	'Unusual equipment',
	'Unexpected age or origin',
])

const LOCATION = toEqualOdds([
	'Typical area',
	'Transitional area',
	'Living area or meeting place',
	'Working or utility area',
	'Area with a special feature',
	'Location for a specialized purpose',
])

const FEATURE = toEqualOdds([
	'Notable structure',
	'Dangerous hazard',
	'A settlement',
	'Strange natural feature',
	'TERRAIN',
	'DUNGEON_CRAWLER',
])

const DIRECTION = toEqualOdds([
	'North',
	'Northeast',
	'East',
	'Southeast',
	'South',
	'Southwest',
	'West',
	'Northwest',
])

const DESERT_LANDFORMS = toEqualOdds([
	'Barchan - a convex-shaped sand dune with a gentle slope up the side of the wind direction and a 30-35 degree slip face that faces away from the wind',
	'Blowout - a small hollow',
	'Desert pavement - a sheet-like surface of rock',
	'Desert varnish - a dark stain on the surfaces of desert rocks',
	'Dreikanter - a three-faced weathered rock',
	'Dry lake - a waterless lakebed, typically covered in fine-grained rocks that contain salt',
	'Dune - a hill or mountain of sand',
	'Erg - a sand covered desert',
	'Loess - an accumulation of sediment or silt that are joined together by calcium carbonate',
	'Sandhill - a sandy or low-vegetation hill area that receives minimal rainfall and has trouble retaining the water',
	'Ventifact - rocks that have been cut and polished by the wind',
	'Yardang - very large or very long, streamlined, sculpted forms caused by wind erosion',
])

const COASTAL_LANDFORMS = toEqualOdds([
	'Arch - a rock formation with an opening',
	'Archipelago - a group of islands',
	'Ayre - a narrow beach across the ends of a shallow bay',
	'Barrier bar and barrier island - a flat formation of sand that is parallel to the coast',
	'Beach and raised beach - the land along the edge of a body of water, consisting of loose rocks or sand',
	'Beach cusps - sediment in an arc shape on the shore, caused by the wave action',
	'Beach ridge - a ridge running parallel to the water\'s edge, caused by wave action',
	'Bight - a recessed area in a coastline',
	'Blowhole - a hole in the end of a sea cave',
	'Calanque - a steep cove',
	'Cape - a portion of land that extends into the sea or ocean',
	'Coast - where the water meets the land',
	'Cove - a small bay',
	'Cuspate foreland - an accumulation of sand and gravel forming a land body that extends like a "finger" into the body of water',
	'Dune system - groups of sand dunes',
	'Firth - a large bay',
	'Fjard - a short, shallow and broad fjord',
	'Fjord - a long narrow inlet with steep cliffs',
	'Headland - a point of land that extends int a body of water and has a steep drop',
	'Island, islet - a portion of land that is surrounded on all sides by water',
	'Isthmus - a narrow strip of land with water on each side',
	'Machair - the grassy fields that are inland from a dune ridge',
	'Mid-ocean ridge - an underwater chain of mountains where upwelling magma forms new crust',
	'Marine terrace - a flat, often slightly inclined, surface with a slight slope on the water side and a steeper slope on the land side',
	'Peninsula - a piece of land that has water on three sides',
	'Sea cave - a cave at the edge of the sea that is formed by wave action',
	'Sea cliff - a vertical wall of rock at the coastline',
	'Shoal - a sandbar',
	'Shore - where the water meets the land',
	'Surge channel - a very narrow opening in the rocks of the shoreline',
	'Wave cut platform - the flat area at the base of a cliff created by the waves',
	'Oceanic trench - a very long steep depression in the ocean floor',
	'Submarine volcano - an underwater vent where magma may erupt',
])

const VOLCANIC_LANDFORMS = toEqualOdds([
	'Caldera - a crater formed by the collapse of land after an eruption',
	'Complex volcano - a volcano with more than one feature because of multiple vents',
	'Crater lake - a lake that formed inside a caldera',
	'Cryovolcano - erupts water, methane or ammonia rather than lava',
	'Geyser - a hole in the ground where water and steam shoots out',
	'Guyot - a volcano under the water with a flat top',
	'Lava dome - a mound that forms by lava that does not easily flow away',
	'Lava flow - lava that is moving',
	'Lava plain - a large area of lava flows',
	'Lava spine - a vertical formation formed by slow-moving lava',
	'Maar - a shallow volcanic crater caused by an explosion of groundwater water contacting magma or lava',
	'Malpais - an area of eroded volcanic rocks',
	'Mud volcano - a mound formed when gas comes through a vent and causes mud to boil',
	'Pit crater - formed by a collapse of the surface rather than an eruption',
	'Sand volcano - made of ejected sand',
	'Shield volcano - a large domed volcano with gently sloping sides',
	'Stratovolcano - a conical volcano with steep sides',
	'Subglacial mound - formed by eruption under a glacier',
	'Supervolcano - a volcano that may eject a volume of more than 240 cubic miles',
	'Tuya - a volcano with steep sides and a flat top made by lava erupting through a glacier',
	'Vent - an opening through which lava, ash and gas can escape',
	'Volcanic arc - a chain of volcanoes positioned in a slightly curved layout',
	'Volcanic dam - a natural dam made of lava or other volcanic material and debris',
	'Volcanic field - an area with a cluster of volcanoes',
	'Volcanic island - a volcano under the ocean that grew until it broke the surface of the water',
	'Volcanic plateau - a flat surface formed by many volcanic eruptions',
	'Volcanic plug - forms when magma cools and hardens within a vent',
	'Volcano - a mountain formed by an eruption of lava and ash',
])

const MOUNTAIN_LANDFORMS = toEqualOdds([
	'Badlands - a dry terrain with steep slopes and little or no vegetation',
	'Butte - an isolated hill that typically has a flat stop and steep sides',
	'Canyon - a deep ravine between two cliffs or encasements, like the Grand Canyon',
	'Cave - an underground space created by the weathering of rocks that is enclosed and large enough to enter',
	'Cliff - an area with a steep drop-off, usually formed by erosion and near rock exposures',
	'Cuesta - a hill or a ridge with a gentle slope',
	'Gulch - a deep valley that has generally been formed by land erosion',
	'Gully - a ditch or valley created by erosion',
	'Hogback - a narrow ridge of hills with steep slopes and a narrow crest; the slopes are usually close to equal on both sides',
	'Hoodoo - a tall thin rock formation protruding from the bottom of a badland',
	'Lavaka - a hole in the side of a hill caused by erosion',
	'Mesa - a tableland, or an elevated area with a flat top and steep cliff-like sides',
	'Mountain pass - a path through a mountain range over a low oint in a ridge',
	'Ravine - formed by running water, a ravine is smaller than a canyon and is steep and deep',
	'Ridge - a chain of hills or mountains',
	'Rock shelter - a cave-like opening at the base of a cliff',
	'Scree - a collection of broken rocks at the base of a mountain',
	'Strath - a wide shallow river valley',
	'Summit - the highest point on a hill or mountain',
	'Valley - a low area between hills or mountains',
])

const CLIMATES = toEqualOdds([
	'TROPICAL',
	'ARID',
	'HUMID_MESOTHERMAL',
	'TIAGA',
	'TUNDRA',
	'HIGHLANDS',
])

const TROPICAL_BIOMES = toEqualOdds([
	'Tropical and subtropical moist broadleaf forests',
	'Tropical and subtropical dry broadleaf forests',
	'Tropical and subtropical coniferous forests',
	'Tropical and subtropical grasslands, savannas and shrublands',
	'Grasslands',
	'Mangroves',
	'Large rivers',
	'Large river headwaters',
	'Large river deltas',
	'Small rivers',
	'Large lakes',
	'Small lake ecosystems',
	'Tropical coasts',
	'Marine',
])

const TROPICAL_LANDFORMS = [
	...VOLCANIC_LANDFORMS,
	...COASTAL_LANDFORMS,
]

const ARID_BIOMES = toEqualOdds([
	'Deserts and xeric shrublands',
	'Xeric basins',
	'Small rivers',
	'Desert coasts',
	'Marine',
])

const ARID_LANDFORMS = [
	...VOLCANIC_LANDFORMS,
	...DESERT_LANDFORMS,
	...COASTAL_LANDFORMS,
	...MOUNTAIN_LANDFORMS,
]

const HUMID_MESOTHERMAL_BIOMES = toEqualOdds([
	'Tropical and subtropical moist broadleaf forests',
	'Tropical and subtropical dry broadleaf forests',
	'Tropical and subtropical coniferous forests',
	'Temperate broadleaf and mixed forests',
	'Temperate coniferous forests',
	'Tropical and subtropical grasslands, savannas and shrublands',
	'Temperate grasslands, savannas, and shrublands',
	'Flooded grasslands and savannas',
	'Montane grasslands and shrublands',
	'Equatorial forests and woodlands',
	'Mangroves',
	'Grasslands',
	'Large rivers',
	'Large river headwaters',
	'Large river deltas',
	'Small rivers',
	'Large lakes',
	'Small lake ecosystems',
	'Tropical coasts',
	'Temperate coasts',
	'Marine',
])

const HUMID_MESOTHERMAL_LANDFORMS = [
	...VOLCANIC_LANDFORMS,
	...COASTAL_LANDFORMS,
	...MOUNTAIN_LANDFORMS,
]

const TIAGA_BIOMES = toEqualOdds([
	'Temperate coniferous forests',
	'Boreal forests and tiaga',
	'Grasslands',
	'Large rivers',
	'Large river headwaters',
	'Large river deltas',
	'Small rivers',
	'Large lakes',
	'Small lake ecosystems',
	'Temperate coasts',
	'Marine',
])

const TIAGA_LANDFORMS = [
	...VOLCANIC_LANDFORMS,
	...COASTAL_LANDFORMS,
	...MOUNTAIN_LANDFORMS,
]

const TUNDRA_BIOMES = toEqualOdds([
	'Small rivers',
	'Small lake ecosystems',
	'Large lakes',
	'Polar coasts',
	'Polar',
	'Marine',
])

const TUNDRA_LANDFORMS = [
	...VOLCANIC_LANDFORMS,
	...COASTAL_LANDFORMS,
	...MOUNTAIN_LANDFORMS,
]

const HIGHLANDS_BIOMES = toEqualOdds([
	'Temperate broadleaf and mixed forests',
	'Temperate coniferous forests',
	'Montane grasslands and shrublands',
	'Xeric basins',
	'Deserts and xeric shrublands',
	'Temperate coasts',
	'Marine',
	'Large rivers',
	'Large river headwaters',
	'Small rivers',
	'Large lakes',
	'Small lake ecosystems',
])

const HIGHLANDS_LANDFORMS = [
	...VOLCANIC_LANDFORMS,
	...COASTAL_LANDFORMS,
	...MOUNTAIN_LANDFORMS,
]

// Custom odds

const IS_ALTERED_SCENE = [
	['Not altered', 0.00],
	['ALTERED_SCENE', 0.83]
]

const IS_RANDOM_EVENT = [
	['None', 0.00],
	['RANDOM_EVENT', 0.98]
]

const YES_NO = {
	'LIKELY': [
		['No', 0.00],
		['Yes', 0.17]
	],
	'EVEN': [
		['No', 0.00],
		['Yes', 0.50]
	],
	'UNLIKELY': [
		['No', 0.00],
		['Yes', 0.83]
	]
}

const MOD = [
	['but...', 0.00],
	['', 0.1],
	['and...', 0.90],
]

const QUANTITY_QUALITY = [
	['Exceedingly less than you expected', 0.00],
	['Much less than you expected', 0.03],
	['A little less than you expected', 0.1],
	['About as much as you expected', 0.3],
	['A little more than you expected', 0.7],
	['Much more than you expected', 0.9],
	['Exceedingly more than you expected', 0.97],
]

const ENCOUNTER = [
	['None', 0.00],
	['Hostile enemies', 0.33],
	['An obstacle blocks the way', 0.67],
	['Unique NPC or adversary', 0.83],
]

const OBJECT = [
	['Nothing, or mundane objects', 0.00],
	['An interesting item or clue', 0.33],
	['A useful tool, key or clue', 0.5],
	['Something valuable', 0.67],
	['Rare or special item', 0.83]
]

const TOTAL_EXITS = [
	['Dead end, or existing exits', 0.00],
	['1 additional exit', 0.33],
	['2 additional exits', 0.67],
]

const TERRAIN = [
	['Same as the current hex', 0.00],
	['Common terrain', 0.33],
	['Uncommon terrain', 0.67],
	['Rare terrain', 0.83],
]

const CONTENTS = [
	['Nothing notable', 0.00],
	['FEATURE', 0.83],
]

const EVENT = [
	['None', 0.00],
	['RANDOM_EVENT', 0.67],
]

const generatePlotHook = () =>
	({
		objective: getRandomItem(OBJECTIVE),
		adversaries: getRandomItem(ADVERSARIES),
		rewards: getRandomItem(REWARDS),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateWithDomain = (focusKey, focusArr) => () =>
	({
		[focusKey]: [
			resolveRandomItem(getRandomItem(focusArr)),
			getRandomItem(SUIT_DOMAIN),
		],
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateAction = generateWithDomain('action', ACTION_FOCUS)
const generateDetail = generateWithDomain('detail', DETAIL_FOCUS)
const generateTopic = generateWithDomain('topic', TOPIC_FOCUS)

const generateRandomEvent = () =>
	({
		action:[
			resolveRandomItem(getRandomItem(ACTION_FOCUS)),
			getRandomItem(SUIT_DOMAIN),
		],
		topic:[
			resolveRandomItem(getRandomItem(TOPIC_FOCUS)),
			getRandomItem(SUIT_DOMAIN),
		]
	})

const generateSceneComplication = () =>
	({
		complication: getRandomItem(SCENE_COMPLICATION),
		altered: resolveRandomItem(getRandomItem(IS_ALTERED_SCENE)),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateYesOrNo = odds =>
	({
		answer: [getRandomItem(YES_NO[odds]), getRandomItem(MOD)],
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateQuantityQuality = () =>
	({
		answer: getRandomItem(QUANTITY_QUALITY),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generatePacingMove = () =>
	({
		answer: resolveRandomItem(getRandomItem(PACING_MOVE)),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateFailureMove = () =>
	({
		answer: resolveRandomItem(getRandomItem(FAILURE_MOVE)),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateGeneric = () =>
	({
		operation:[
			resolveRandomItem(getRandomItem(ACTION_FOCUS)),
			getRandomItem(SUIT_DOMAIN),
		],
		appearance:[
			resolveRandomItem(getRandomItem(DETAIL_FOCUS)),
			getRandomItem(SUIT_DOMAIN),
		],
		significance: getRandomItem(QUANTITY_QUALITY),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateIdentity = generateWithDomain('identity', IDENTITY)
const generateGoal = generateWithDomain('goal', GOAL)

const generateNPC = () =>
	({
		identity:[
			resolveRandomItem(getRandomItem(IDENTITY)),
			getRandomItem(SUIT_DOMAIN),
		],
		goal:[
			resolveRandomItem(getRandomItem(GOAL)),
			getRandomItem(SUIT_DOMAIN),
		],
		feature:[
			getRandomItem(NOTABLE_FEATURES),
			[
				getRandomItem(DETAIL_FOCUS),
				getRandomItem(SUIT_DOMAIN)
			]
		],
		attitude: `${getRandomItem(QUANTITY_QUALITY)}`,
		conversation: [resolveRandomItem(getRandomItem(TOPIC_FOCUS)), getRandomItem(SUIT_DOMAIN)],
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateDungeonCrawler = () =>
	({
		operation:[
			resolveRandomItem(getRandomItem(ACTION_FOCUS)),
			getRandomItem(SUIT_DOMAIN),
		],
		appearance:[
			resolveRandomItem(getRandomItem(DETAIL_FOCUS)),
			getRandomItem(SUIT_DOMAIN),
		],
		location: getRandomItem(LOCATION),
		encounter: getRandomItem(ENCOUNTER),
		object: getRandomItem(OBJECT),
		exits: getRandomItem(TOTAL_EXITS),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateRegion = () => {
	const climate = getRandomItem(CLIMATES)
	return ({
		...resolveRegion(getRandomItem(CLIMATES)),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})
}

const generateDirection = () =>
	({
		direction: getRandomItem(DIRECTION),
		event: resolveRandomItem(getRandomItem(IS_RANDOM_EVENT)),
	})

const generateHex = () =>
	({
		terrain: getRandomItem(TERRAIN),
		contents: resolveRandomItem(getRandomItem(CONTENTS)),
		features: resolveRandomItem(getRandomItem(FEATURE)),
		event: resolveRandomItem(getRandomItem(EVENT)),
	})

const rollFateDie = () =>
	([-1, 0, 1][Math.floor(Math.random()*3)])

const toFateSymbol = n =>
	n < 0 ? '-' :
		n > 0 ? '+' : ' '

const generateFateRoll = () => {
	const results = [rollFateDie(), rollFateDie(), rollFateDie(), rollFateDie()]
	const roll = results.map(toFateSymbol)
	return ({
		roll,
		result: results.reduce((x, y) => x + y, 0)
	})
}

function resolveRegion(climate) {
	switch(climate) {
		case 'TROPICAL':
			return ({
				biome: getRandomItem(TROPICAL_BIOMES),
				features: [
					getRandomItem(TROPICAL_LANDFORMS),
					getRandomItem(TROPICAL_LANDFORMS),
				]
			})
		case 'ARID':
			return ({
				biome: getRandomItem(ARID_BIOMES),
				features: [
					getRandomItem(ARID_LANDFORMS),
					getRandomItem(ARID_LANDFORMS),
				]
			})
		case 'HUMID_MESOTHERMAL':
			return ({
				biome: getRandomItem(HUMID_MESOTHERMAL_BIOMES),
				features: [
					getRandomItem(HUMID_MESOTHERMAL_LANDFORMS),
					getRandomItem(HUMID_MESOTHERMAL_LANDFORMS),
				]
			})
		case 'TIAGA':
			return ({
				biome: getRandomItem(TIAGA_BIOMES),
				features: [
					getRandomItem(TIAGA_LANDFORMS),
					getRandomItem(TIAGA_LANDFORMS),
				]
			})
		case 'TUNDRA':
			return ({
				biome: getRandomItem(TUNDRA_BIOMES),
				features: [
					getRandomItem(TUNDRA_LANDFORMS),
					getRandomItem(TUNDRA_LANDFORMS),
				]
			})
		case 'HIGHLANDS':
			return ({
				biome: getRandomItem(HIGHLANDS_BIOMES),
				features: [
					getRandomItem(HIGHLANDS_LANDFORMS),
					getRandomItem(HIGHLANDS_LANDFORMS),
				]
			})
		default: return climate
	}
}

function resolveRandomItem(item) {
	switch(item) {
		case 'ALTERED_SCENE': return resolveRandomItem(getRandomItem(ALTERED_SCENE))
		case 'RANDOM_EVENT': return generateRandomEvent()
		case 'SCENE_COMPLICATION': return getRandomItem(SCENE_COMPLICATION)
		case 'PACING_MOVE': return resolveRandomItem(getRandomItem(PACING_MOVE))
		case 'FAILURE_MOVE': return resolveRandomItem(getRandomItem(FAILURE_MOVE))
		case 'FEATURE': return resolveRandomItem(getRandomItem(FEATURE))
		case 'TERRAIN': return getRandomItem(TERRAIN)
		case 'DUNGEON_CRAWLER': return generateDungeonCrawler()
		default: return item
	}
}
