const el = tag =>
	document.createElement(tag)

const button = (label, action) => {
	const btn = el('button')
	btn.innerHTML = label
	btn.onclick = action
	return btn
}

const card = (title, ...children) => {
	const container = el('div')
	const content = el('div')
	const label = el('h2')
	label.append(document.createTextNode(title))
	container.classList.add('card')
	content.classList.add('card-content')
	content.append(...children)
	container.append(label, content)
	return container
}

const container = el('div')
container.classList.add('container')
const alertOverlay = el('div')
alertOverlay.classList.add('alert-overlay')
alertOverlay.classList.add('hide')
const closeBtn = el('div')
closeBtn.append(document.createTextNode('✕'))
closeBtn.classList.add('close-button')
closeBtn.onclick = () =>
	alertOverlay.classList.add('hide')
const copied = el('div')
copied.classList.add('copied')
copied.innerHTML = 'copied!'
const alertContent = el('div')
alertContent.title = 'click to copy result'
alertContent.classList.add('alert-content')
alertContent.onclick = e => {
	navigator.clipboard.writeText(alertContent.rawContent)
		.then(() => {
			alertContent.append(copied)
			console.log('Async: Copying to clipboard was successful!', alertContent.rawContent);
		})
		.catch(err => console.error('Async: Could not copy text: ', err))
}
alertOverlay.append(alertContent, closeBtn)
alertOverlay.onclick = e => {
	if(e.target === e.currentTarget) {
		alertOverlay.classList.add('hide')
	}
}
const rulesLink = el('a')
rulesLink.classList.add('button')
rulesLink.href = 'https://inflatablestudios.itch.io/one-page-solo-engine'
rulesLink.target = '_blank'
rulesLink.append(document.createTextNode('OFFICIAL RULES'))

const alertResult = (action, result, rawResult) => {
	alertContent.innerHTML = ''
	const alertTitle = el('div')
	alertTitle.classList.add('title')
	alertTitle.append(document.createTextNode(action))
	alertContent.rawContent = rawResult
	alertContent.append(alertTitle, result)
	alertOverlay.classList.remove('hide')
}

const isObject = x =>
	(x && typeof x === 'object' && x.constructor === Object)

const displayResult = result => {
	const container = el('div')
	container.classList.add('result-display')
	if(isObject(result)) {
		container.append(
			...Object.entries(result)
				.map(([key, value]) => {
					const item = el('div')
					item.classList.add('item')
					const label = el('div')
					label.classList.add('label')
					label.append(document.createTextNode(key))
					const result = displayResult(value)
					result.classList.add('value')
					item.append(label, result)
					return item
				})
		)
	} else if(Array.isArray(result)) {
		container.append(
			...result.map(displayResult)
		)
	} else {
		container.append(document.createTextNode(`- ${result}`))
	}
	return container
}

const capitalize = str =>
	`${str[0].toUpperCase()}${str.slice(1).toLowerCase()}`

const formatRawResult = result => {
	let text = ''
	if(isObject(result)) {
		text += Object.entries(result)
				.map(([key, value]) => `${capitalize(key)}: ${formatRawResult(value)}`).join('\n')
	} else if(Array.isArray(result)) {
		text += result.map(formatRawResult).join(', ')
	} else {
		text += result
	}
	return text
}

const createAction = (action, generateResult) => () => {
	const result = generateResult()
	alertResult(action, displayResult(result), formatRawResult(result))
}

const toDie = sym => {
	const die = el('div')
	die.classList.add('die')
	die.append(document.createTextNode(sym))
	return die
}

const createRollAction = (container, generateResult) => () => {
	container.innerHTML = ''
	const { roll, result } = generateResult()
	const dice = roll.map(toDie)
	const resultDisplay = el('div')
	resultDisplay.classList.add('dice-result')
	resultDisplay.append(document.createTextNode(result))
	container.append(...dice, resultDisplay)
}


const header = () => {
	const desc = el('p')
	desc.innerHTML = 'A minimal all-in-one toolkit to play your favorite TTRPGs without a GM.'
	return card('One Page Solo Engine', desc)
}

const ruleSetScene = () => {
	const desc = el('p')
	desc.innerHTML = 'Describe where your character is and what they are trying to accomplish, then generate a complication:'
	return card('Set The Scene', desc, button('SCENE COMPLICATION', createAction('SCENE COMPLICATION', generateSceneComplication)))
}

const ruleAskYesNo = () => {
	const desc = el('p')
	desc.innerHTML = 'When you need to ask a simple question, choose the likelihood and click:'
	return card(
		'Ask a "Yes" or "No" Question',
		desc,
		button('ASK LIKELY', createAction('LIKELY YES/NO', () => generateYesOrNo('LIKELY'))),
		button('ASK EVEN', createAction('EVEN YES/NO', () => generateYesOrNo('EVEN'))),
		button('ASK UNLIKELY', createAction('UNLIKELY YES/NO', () => generateYesOrNo('UNLIKELY'))),
	)
}

const ruleAskQuantityQuality = () => {
	const desc = el('p')
	desc.innerHTML = 'When you need to know how big, good, strong, numerous, etc. something is:'
	return card(
		'Ask About Quality or Quantity',
		desc,
		button('ASK QUALITY/QUANTITY', createAction('QUALITY/QUANTITY', generateQuantityQuality)),
	)
}

const ruleGmMoves = () => {
	const desc = el('p')
	desc.innerHTML = 'When you need to advance the action, generate from the buttons below and describe the results as the GM normally would.'
	return card(
		'Ask "What now?" or "What consequence?"',
		desc,
		button('ASK WHAT NOW?', createAction('WHAT NOW?', generatePacingMove)),
		button('ASK WHAT CONSEQUENCE?', createAction('WHAT CONSEQUENCE?', generateFailureMove)),
	)
}

const ruleComplexQuestion = () => {
	const desc = el('p')
	desc.innerHTML = 'When you need to ask an open-ended question, find the most appropriate question.'
	return card(
		'Complex Questions',
		desc,
		button('WHAT DOES IT DO?', createAction('WHAT DOES IT DO?', generateAction)),
		button('WHAT KIND OF THING IS IT?', createAction('WHAT KIND OF THING IS IT?', generateDetail)),
		button('WHAT IS THIS ABOUT?', createAction('WHAT IS THIS ABOUT?', generateTopic)),
	)
}

const ruleRandomEvent = () => {
	return card(
		'Random Event',
		button('GENERATE RANDOM EVENT', createAction('RANDOM EVENT', generateRandomEvent)),
	)
}

const ruleGenericGenerator = () => {
	const desc = el('p')
	desc.innerHTML = 'Use this to generate towns, spaceships, factions, magic items, taverns, monsters or anything else you can think of.'
	return card(
		'Generic Generator',
		desc,
		button('GENERATE GENERIC', createAction('GENERATE GENERIC', generateGeneric)),
	)
}

const rulePlotHook = () => {
	const desc = el('p')
	desc.innerHTML = 'Use this to generate plot hooks, quests, or missions for the PCs to follow.'
	return card(
		'Plot Hook Generator',
		desc,
		button('GENERATE PLOT HOOK', createAction('GENERATE PLOT HOOK', generatePlotHook)),
	)
}

const ruleNPC = () => {
	const desc = el('p')
	desc.innerHTML = 'Use this to generate NPCs that may be encountered while playing.'
	return card(
		'NPC Generator',
		desc,
		button('GENERATE NPC', createAction('GENERATE NPC', generateNPC)),
	)
}

const ruleDungeonCrawler = () => {
	const desc = el('p')
	desc.innerHTML = 'Use this when exploring a dangerous location like a typical dungeon.'
	return card(
		'Dungeon Crawler',
		desc,
		button('GENERATE DUNGEON', createAction('GENERATE DUNGEON', generateDungeonCrawler)),
	)
}

const ruleRegion = () => {
	const desc = el('p')
	desc.innerHTML = 'Use this to generate an outdoor environment. Regenerate if the region doesn\'t make sense.'
	return card(
		'Generate Region',
		desc,
		button('GENERATE REGION', createAction('GENERATE REGION', generateRegion)),
	)
}

const ruleHex = () => {
	const desc = el('p')
	desc.innerHTML = 'After generating a REGION, use this to determine what the immediate vicinity around the PCs is.'
	return card(
		'Generate Local Zone',
		desc,
		button('GENERATE Zone', createAction('GENERATE ZONE', generateHex)),
	)
}

const ruleDirection = () => {
	return card(
		'Generate Direction',
		button('GENERATE DIRECTION', createAction('GENERATE DIRECTION', generateDirection)),
	)
}

const ruleFateDice = () => {
	const characterSheetLink = el('a')
	characterSheetLink.href = 'fate/index.html'
	characterSheetLink.target = '_blank'
	characterSheetLink.classList.add('button')
	characterSheetLink.append(document.createTextNode('CHARACTER SHEETS'))
	const rollDisplay = el('div')
	rollDisplay.classList.add('roll-display')
	createRollAction(rollDisplay, () => ({roll:[' ', ' ', ' ', ' '], result:0}))()
	rollDisplay.onclick = createRollAction(rollDisplay, generateFateRoll)
	return card(
		'Fate Helpers',
		characterSheetLink,
		rollDisplay
	)
}

const ruleDiceRoller = () => {
	const container = el('div')
	container.className = 'random-num-container'
	const diceCount = el('input')
	diceCount.placeholder = 'Input Number of Dice [default 1]'
	const dieSides = el('input')
	dieSides.placeholder = 'Input Die Sides [default 20]'
	window.addEventListener('keyup', e => {
		if(e.key === 'r') {
			dieSides.focus()
		} else if(e.key === 'Enter' && !(Array.from(alertOverlay.classList).includes('hide'))) {
			alertOverlay.classList.add('hide')
		}
	})
	dieSides.addEventListener('keyup', e => {
		if(e.key === 'Enter') {
			e.stopPropagation()
			const count = Number(diceCount.value !== '' && !isNaN(diceCount.value) ? diceCount.value : 1)
			const sides = Number(dieSides.value !== '' && !isNaN(dieSides.value) ? dieSides.value : 20)
			const result = Array.from(Array(count)).map(() => getRandomInt(1, sides)).reduce((x, y) => x + y, 0)
			alertResult(`Rolled a D${sides}`, displayResult(result), formatRawResult(result))
			dieSides.blur()
		}
	})
	container.append(diceCount, dieSides)
	return card(
		'Dice Roller',
		container,
		button('Roll', () => {
			const count = Number(diceCount.value !== '' && !isNaN(diceCount.value) ? diceCount.value : 1)
			const sides = Number(dieSides.value !== '' && !isNaN(dieSides.value) ? dieSides.value : 20)
			const result = Array.from(Array(count)).map(() => getRandomInt(1, sides)).reduce((x, y) => x + y, 0)
			diceCount.value = ''
			dieSides.value = ''
			alertResult(`Rolled a D${sides}`, displayResult(result), formatRawResult(result))
		})
	)
}

const ruleRandomNumberGenerator = () => {
	const container = el('div')
	container.className = 'random-num-container'
	const minInput = el('input')
	minInput.placeholder = 'Input Minimum [default 0]'
	const maxInput = el('input')
	maxInput.placeholder = 'Input Maximum [default 100]'
	container.append(minInput, maxInput)
	return card(
		'Random Number',
		container,
		button('GENERATE Random Number', () => {
			const min = Number(minInput.value !== '' && !isNaN(minInput.value) ? minInput.value : 0)
			const max = Number(maxInput.value !== '' && !isNaN(maxInput.value) ? maxInput.value : 100)
			const result = getRandomInt(min, max)
			minInput.value = ''
			maxInput.value = ''
			alertResult(`Random Number Between ${min} and ${max}`, displayResult(result), formatRawResult(result))
		})
	)
}

container.append(
	rulesLink,
	alertOverlay,
	ruleFateDice(),
	ruleRandomNumberGenerator(),
	ruleDiceRoller(),
	ruleSetScene(),
	ruleAskYesNo(),
	ruleAskQuantityQuality(),
	ruleGmMoves(),
	ruleComplexQuestion(),
	ruleRandomEvent(),
	ruleGenericGenerator(),
	rulePlotHook(),
	ruleNPC(),
	ruleDungeonCrawler(),
	ruleRegion(),
	ruleHex(),
	ruleDirection(),
)

document.body.append(container)
