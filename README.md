# Solo RPG Tools

The GM/Generators are using a modified version of [One Page Solo Engine](https://inflatablestudios.itch.io/one-page-solo-engine)
# Generator Tools

(Dark and light modes use system preferences)

![generator tool](./generator.png)

## To use:

	Open the `index.html` file in your browser (no server needed).

# Fate Condensed Character Sheet

(Dark and light modes use system preferences)

![character sheet tool](./character-sheet.png)

## To use:

	Open the `fate/index.html` file in your browser

### Notes on data persistence

Character sheets are saved in your browser cache (localStorage) after every
edit. So if you clear your browser cache, you could lose your character sheets.
I recommend using the `export` feature to generate a `JSON` file to save to your
character sheets to local disk, for importing later.

The `export` feature exports all the data for all character sheets in a single
file.

