let selectedSheet

const el = tag =>
	document.createElement(tag)

const mainContainer = el('main')
mainContainer.classList.add('container')

const buildButton = (label, action) => {
	const btn = el('button')
	btn.onclick = action
	btn.innerHTML = label
	return btn
}

const buildTitle = title => {
	const titleEl = el('div')
	titleEl.innerHTML = title
	titleEl.classList.add('title')
	return titleEl
}

const buildLabeledInput = (label, value='', action) => {
	const container = el('div')
	container.classList.add('labeled-input-container')
	const labelEl = el('div')
	labelEl.innerHTML = label
	labelEl.classList.add('input-label')
	const input = el('input')
	input.value = value
	input.onchange = e => action(e.target.value)
	container.append(labelEl, input)
	return container
}

const buildLabeledTextarea = (label, value='', action) => {
	const container = el('div')
	container.classList.add('labeled-input-container')
	const labelEl = el('div')
	labelEl.innerHTML = label
	labelEl.classList.add('input-label')
	const input = el('textarea')
	input.value = value
	input.onchange = e => action(e.target.value)
	container.append(labelEl, input)
	return container
}

const buildSection = (title, buttons=[], children=[]) => {
	const container = el('div')
	container.classList.add('section-container')
	container.id = `section-${title}`
	const header = el('div')
	header.classList.add('section-header')
	const titleEl = buildTitle(title)
	const content = el('div')
	content.classList.add('section-content')
	header.append(titleEl)
	if(buttons.length) {
		header.append(...buttons)
	}
	container.append(header)
	if(children.length) {
		content.append(...children)
		container.append(content)
	}
	return container
}

const buildSkill = (name, value, action) => {
	const container = el('div')
	container.classList.add('skill-container')
	const input = el('input')
	input.type = 'number'
	input.value = value
	input.onchange = e => action(e.target.value)
	input.classList.add('skill-input')
	const label = el('div')
	label.innerHTML = name
	label.classList.add('skill-label')
	container.append(input, label)
	return container
}

const buildRefreshInput = (name, value, action) => {
	const container = el('div')
	container.classList.add('refresh-input-container')
	const input = el('input')
	input.type = 'number'
	input.value = value
	input.onchange = e => action(e.target.value)
	input.classList.add('refresh-input')
	const label = el('div')
	label.innerHTML = name
	label.classList.add('refresh-label')
	container.append(label, input)
	return container
}

const buildStunt = (action, remove) => ({name, description}, i) => {
	const container = el('div')
	container.classList.add('stunt-container')
	const inputs = el('div')
	inputs.classList.add('stunt-inputs')
	const nameInput = buildLabeledInput(
		'name',
		name,
		value => action({id:i, key:'name', value})
	)
	const descInput = buildLabeledTextarea(
		'description',
		description,
		value => action({id:i, key:'description', value})
	)
	inputs.append(nameInput, descInput)
	const removeBtn = buildButton('remove', () => remove(i))
	container.append(inputs, removeBtn)
	return container
}

const buildStressCheckbox = action => (value, i) => {
	const checkbox = el('div')
	checkbox.classList.add('checkbox')
	checkbox.index = i
	if(value === 0) {
		checkbox.classList.add('unselected')
	} else if(value === 1) {
		checkbox.classList.add('selected')
	} else if(value === 'disabled') {
		checkbox.classList.add('disabled')
	}
	checkbox.onclick = e => {
		let newVal
		if(value === 0) {
			newVal = 1
		} else if(value === 1) {
			newVal = 0
		} else if(value === 'disabled') {
			newVal = 'disabled'
		}
		action({value:newVal, id:i})
	}
	return checkbox
}

const buildStress = (name, values, action) => {
	const onclick = e => {
		action(e.target.index)
	}
	const container = el('div')
	container.classList.add('stress-container')
	const label = el('div')
	label.innerHTML = name
	label.classList.add('stress-label')
	const checkboxContainer = el('div')
	checkboxContainer.classList.add('stress-checkbox-container')
	checkboxContainer.append(
		...values
			.map(buildStressCheckbox(change => {
				let vals = values.filter((_, i) => i !== change.id)
				action([
					...values.slice(0, change.id),
					change.value,
					...values.slice(change.id + 1)
				])
			}))
	)
	container.append(label, checkboxContainer)
	return container
}

const newEmptySheet = () =>
	({
		name:'',
		refresh:3,
		current:3,
		highConcept:'',
		trouble:'',
		relationship:'',
		aspect1:'',
		aspect2:'',
		academics: 0,
		athletics: 0,
		burglary: 0,
		contacts: 0,
		crafts: 0,
		deceive: 0,
		drive: 0,
		empathy: 0,
		fight: 0,
		investigate: 0,
		lore: 0,
		notice: 0,
		physique: 0,
		provoke: 0,
		rapport: 0,
		resources: 0,
		shoot: 0,
		stealth: 0,
		will: 0,
		physicalStress:[0, 0, 0, 'disabled', 'disabled', 'disabled'],
		mentalStress:[0, 0, 0, 'disabled', 'disabled', 'disabled'],
		mild:'',
		moderate:'',
		severe:'',
		stunts:[]
	})

const newEmptyStunt = () =>
	({
		name:'',
		description:''
	})

const SaveSheet = () => {
	const sheets = (JSON.parse(localStorage.characterSheets) || [])
		.filter(({name}) => name !== selectedSheet.name)
	sheets.unshift(selectedSheet)
	localStorage.setItem('characterSheets', JSON.stringify(sheets))
	render(selectedSheet)
}

const Update = key => value => {
	selectedSheet[key] = value
	if(key === 'physique') {
		if(value >= 3) {
			selectedSheet.physicalStress =
				[...selectedSheet.physicalStress]
					.map(value => value === 'disabled' ? 0 : value)
		} else {
			selectedSheet.physicalStress = [
					...selectedSheet.physicalStress.slice(0, 3),
					'disabled', 'disabled', 'disabled'
				]
		}
	} else if(key === 'will') {
		if(value >= 3) {
			selectedSheet.mentalStress =
				[...selectedSheet.mentalStress]
					.map(value => value === 'disabled' ? 0 : value)
		} else {
			selectedSheet.mentalStress = [
					...selectedSheet.mentalStress.slice(0, 3),
					'disabled', 'disabled', 'disabled'
				]
		}
	}
	SaveSheet()
	render()
}

const SwitchCharacterSheet = sheetName => {
	console.log('sheet name', sheetName)
	if(sheetName) {
		const sheets = JSON.parse(localStorage.characterSheets) || []
		selectedSheet = sheets.find(sheet => sheet.name === sheetName)
		SaveSheet()
	} else {
		selectedSheet = null
	}
	render(selectedSheet)
}

const NewSheet = () => {
	selectedSheet = newEmptySheet()
	render(selectedSheet)
}

const ImportSheets = e => {
	const reader = new FileReader()
	reader.addEventListener('load', e => {
		localStorage.characterSheets = e.target.result
		render()
	})
	reader.readAsText(e.target.files[0])
}

const ExportSheets = () => {
	let dataStr = localStorage.characterSheets
	let dataUri = 'data:application/json;charset=utf-8,'+encodeURIComponent(dataStr)
	let exportFileDefaultName = 'characterSheets.json'
	let el = document.createElement('a')
	el.setAttribute('href', dataUri)
	el.setAttribute('download', exportFileDefaultName)
	el.click()
}

const CloseSheet = () => {
	selectedSheet = null
	render(JSON.parse(localStorage.characterSheets))
}

const DeleteSheet = () => {
	const sheets = (JSON.parse(localStorage.characterSheets) || [])
		.filter(({name}) => name !== selectedSheet.name)
	selectedSheet = null
	localStorage.setItem('characterSheets', JSON.stringify(sheets))
	render()
}

const NewStunt = () => {
	selectedSheet.stunts.push(newEmptyStunt())
	SaveSheet()
	render(selectedSheet)
}

const UpdateStunt = ({id, key, value}) => {
	const stunt = selectedSheet.stunts.find((_, i) => id === i)
	stunt[key] = value
	selectedSheet.stunts = [
		...selectedSheet.stunts.slice(0, id),
		stunt,
		...selectedSheet.stunts.slice(id + 1),
	]
	SaveSheet()
	render(selectedSheet)
}

const RemoveStunt = id => {
	selectedSheet.stunts = [
		...selectedSheet.stunts.slice(0, id),
		...selectedSheet.stunts.slice(id + 1),
	]
	SaveSheet()
	render(selectedSheet)
}

const buildRefresh = (data, action) => {
	const container = el('div')
	container.classList.add('refresh-container')
	const ref = buildRefreshInput('refresh', data.refresh, action('refresh'))
	const cur = buildRefreshInput('current', data.current, action('current'))
	container.append(ref, cur)
	return container
}

const buildSheet = data => {
	const container = el('div')
	container.classList.add('sheet')

	const headerSection = buildSection(
		'character',
		[
			buildButton('close', CloseSheet),
			buildButton('delete', DeleteSheet),
		],
		[
			buildLabeledInput('name', data.name, Update('name')),
			buildRefresh(data, Update),
		]
	)

	const aspectSection = buildSection(
		'aspects',
		[],
		[
			buildLabeledInput('high concept', data.highConcept, Update('highConcept')),
			buildLabeledInput('trouble', data.trouble, Update('trouble')),
			buildLabeledInput('relationship', data.relationship, Update('relationship')),
			buildLabeledInput('aspect', data.aspect1, Update('aspect1')),
			buildLabeledInput('aspect', data.aspect2, Update('aspect2')),
		]
	)

	const skillsSection = buildSection(
		'skills',
		[],
		[
			buildSkill('academics', data.academics, Update('academics')),
			buildSkill('athletics', data.athletics, Update('athletics')),
			buildSkill('burglary', data.burglary, Update('burglary')),
			buildSkill('contacts', data.contacts, Update('contacts')),
			buildSkill('crafts', data.crafts, Update('crafts')),
			buildSkill('deceive', data.deceive, Update('deceive')),
			buildSkill('drive', data.drive, Update('drive')),
			buildSkill('empathy', data.empathy, Update('empathy')),
			buildSkill('fight', data.fight, Update('fight')),
			buildSkill('investigate', data.investigate, Update('investigate')),
			buildSkill('lore', data.lore, Update('lore')),
			buildSkill('notice', data.notice, Update('notice')),
			buildSkill('physique', data.physique, Update('physique')),
			buildSkill('provoke', data.provoke, Update('provoke')),
			buildSkill('rapport', data.rapport, Update('rapport')),
			buildSkill('resources', data.resources, Update('resources')),
			buildSkill('shoot', data.shoot, Update('shoot')),
			buildSkill('stealth', data.stealth, Update('stealth')),
			buildSkill('will', data.will, Update('will')),
		]
	)

	const stuntsSection = buildSection(
		'stunts',
		[
			buildButton('add', NewStunt),
		],
		data.stunts.map(buildStunt(UpdateStunt, RemoveStunt))
	)

	const vitalsSection = buildSection(
		'vitals',
		[],
		[
			buildStress('physical', data.physicalStress, Update('physicalStress')),
			buildStress('mental', data.mentalStress, Update('mentalStress')),
			buildLabeledInput('mild', data.mild, Update('mild')),
			buildLabeledInput('moderate', data.moderate, Update('moderate')),
			buildLabeledInput('severe', data.severe, Update('severe')),
		]
	)

	container.append(
		headerSection,
		aspectSection,
		skillsSection,
		stuntsSection,
		vitalsSection,
	)

	return container
}

const buildOption = selection => option => {
	const opt = el('option')
	opt.innerHTML = option
	opt.value = option
	if(option === selection) {
		opt.selected = true
	}
	return opt
}

const buildSheetSelector = (sheets=[], selection, action) => {
	const container = el('div')
	container.classList.add('select-container')
	const label = el('div')
	label.innerHTML = 'select character'
	label.classList.add('select-label')
	const select = el('select')
	select.append(
		...[''].concat(sheets.map(sheet => sheet.name))
			.map(buildOption(selection))
	)
	select.onchange = e => action(e.target.value)
	container.append(select, label)
	return container
}

const buildFileInput = action => {
	const container = el('div')
	container.classList.add('file-input-container')
	const buttonWrap = el('div')
	const label = el('label')
	label.classList.add('button')
	label.for = 'upload'
	label.innerHTML = 'import'
	const input = el('input')
	input.style.display = 'none'
	input.id = 'upload'
	input.type = 'file'
	input.accept = '.json'
	label.append(input)
	input.onchange = e => {
		console.log('file input change', e)
		action(e)
	}
	label.onclick = () => input.click()
	buttonWrap.append(label, input)
	container.append(buttonWrap)
	return container
}

function render(data) {
	mainContainer.innerHTML = ''

	mainContainer.append(
		buildSection('menu', [
			buildSheetSelector(
				JSON.parse(localStorage.characterSheets),
				(selectedSheet ? selectedSheet.name : ''),
				SwitchCharacterSheet
			),
			buildButton('new character', NewSheet),
			buildButton('export', ExportSheets),
			buildFileInput(ImportSheets),
		], [])
	)
	if(selectedSheet) mainContainer.append(buildSheet(selectedSheet))
}

const init = () => {
	if(!localStorage.characterSheets) localStorage.setItem('characterSheets', '[]')
	mainContainer.append(selectedSheet)
	document.body.append(mainContainer)
	console.log(localStorage.characterSheets)
	render(JSON.parse(localStorage.characterSheets))
}

window.onload = init
